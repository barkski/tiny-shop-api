## Tiny shop API

A tiny RESTful web service for managing shop's orders.

### Requirements

* Mysql 5.6+
* PHP 7.3+
* Composer
* Symfony binary: https://symfony.com/download

### Installation

Unfortunately I didn't make tests and "dockerization" in time. I wanted to make it perfect with a combination with CI/CD
using gitlab.com, but I failed. So, if you are ok with installing php and mysql on your machine you can do it with there steps:

```
git clone https://gitlab.com/brms994/tiny-shop-api.git
cd tiny-shop-api
```

Install dependencies:
```
composer install
```

Copy `.env` file with `.env.local` name.
Change `DATABASE_URL` to your mysql database.
Example: `DATABASE_URL=mysql://root:pass@127.0.0.1:3306/my_new_database`

```
bin/console doctrine:database:create
bin/console doctrine:migrations:migrate
symfony server:start
```

### Usage

There is no documentation for API yet. You can use http request examples in `requests` folder.

### Assumptions during development

Some assumptions are made in commentaries, but big decisions will be described below.

##### Colors, sizes, types

I already have experience in developing systems for selling clothes (it was about reselling it from
chinese clothes sites) and I know how hard it is to define standards for colors, sizes and types.
So I won't create complex system for these things right now.
I guess I was not supposed to make emphasis here but I want to mention it.

So for simplicity I'll store all these types as strings. We will add indexes or transfer data
to another tables later as it will be needed.

I assume that if product has no size or color than it should be created with "no_size", "no_color" strings.
Instead of "null" it would be like "no_value" value.
It's it mostly because of MySQL checking for uniqueness ignoring null values.

As I developed last API method with getting orders by product type I realized that I need slug for product types for
building nice requests to our API. Now, if product type contains 2 words it won't look good in URL.
For simplicity I won't fix it now, but if I would then product type values should be moved to another table where
we could specify slug for each type in another column.

##### Searching orders by product type

As order table and product type are not directly related I need to make a decision how to relate them.
There are multiple possible ways to determine how we will do it and this should be discussed with product owner.
But as I have no opportunity to do this I'll just say that we will get **order** by **product type** only when we
have _at least one_ **product** (in this order) with requested **product type**.

As we will search by product type field I add index on **product_type** column in **products** table.

##### API exceptions

By default all error are returned with html content-type. I added a feature that make responses to be in json format.
API is now more consistent and it will be easier to work with.

##### Service must perform origin country resolution using a web service and store country code together with the order draft. Because network is unreliable and services tend to fail, let's agree on default country code - "US".

I see requirement about checking country on backend not the best.

It will be not a good idea to add unstable check for country to our high-load pipeline where we count
requests by seconds. Checking country by **IP** isn't the best idea either, because user can use proxy or vpn,
and our order will contain incorrect data even if user didn't want it to be.
I think the best idea will be to move resolving country to frontend.
But I'll do it on back-end anyway because it's a test task!

##### N orders / second are received from a single country (essentially we want to limit number of orders coming from a country in * a given timeframe)

There are several ways to accomplish this kind of limiting. We could use some throttling like this bundle
https://github.com/sunspikes/php-ratelimiter, or create something like it with memcache. But as I can see from
the description of the test task: we need to limit the number of **orders**, not requests to our API. So I'll just
make another validation rule for this requirement. We would need indexes by country and creation date to make
this validation rule fast enough. I have to make sure if indexes will be build by datetime without microseconds
otherwise it won't work.

One more thing to assume is how to store data about limits by country. I think the best idea will be to create
new country table with country code and limit fields. Data will be managed via admin panel
(as will products, colors and sizes). But for simplicity I'll just put it in a constant.
