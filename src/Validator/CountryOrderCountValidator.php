<?php

namespace App\Validator;

use App\Entity\Order;
use App\Helper\CountryHelper;
use App\Repository\OrderRepository;
use DateTime;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CountryOrderCountValidator extends ConstraintValidator
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @inheritDoc
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$value instanceof Order) {
            return;
        }

        if (!$value->getCountryCode()) {
            return;
        }

        $count = $this->orderRepository->getCountByCreateDateAndCountry(new DateTime(), $value->getCountryCode());
        if ($count > CountryHelper::getLimitByCountry($value->getCountryCode())) {
            $this->context->buildViolation($constraint->message)
                ->atPath('')
                ->addViolation();
        }
    }
}