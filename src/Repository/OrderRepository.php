<?php

namespace App\Repository;

use App\Entity\Order;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

class OrderRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * @param string $productType
     * @return Order[]
     */
    public function findByProductType(string $productType)
    {
        return $this
            ->createQueryBuilder('o')
            ->select('o')
            ->innerJoin('o.orderItems', 'o_items')
            ->innerJoin('o_items.product', 'p')
            ->andWhere('p.productType = :product_type')
            ->setParameter('product_type', $productType)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param DateTime $createdAt
     * @param string $countryCode
     * @return int
     */
    public function getCountByCreateDateAndCountry(DateTime $createdAt, string $countryCode)
    {
        try {
            return $this
                ->createQueryBuilder('o')
                ->select('count(o.id)')
                ->andWhere('o.countryCode = :country_code')
                ->andWhere('o.createdAt = :created_at')
                ->setParameter('country_code', $countryCode)
                ->setParameter('created_at', $createdAt)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException $e) {
            // todo: Should log here. Will add monolog later.

            return 0;
        }
    }
}
