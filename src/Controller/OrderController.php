<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Product;
use App\Form\OrderType;
use App\Helper\CountryHelper;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/orders", name="orders_")
 */
class OrderController extends BaseApiController
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param OrderRepository $orderRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        OrderRepository $orderRepository
    ) {
        parent::__construct($entityManager);

        $this->orderRepository = $orderRepository;
    }

    /**
     * @Route("/calculate", name="calculate", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function calculate(Request $request): JsonResponse
    {
        $countyCode = CountryHelper::getCountryByRequest($request) ?? CountryHelper::DEFAULT_COUNTRY_CODE;

        $newEntity = new Order();
        $newEntity->setCountryCode($countyCode);

        return $this->create($request, $newEntity, OrderType::class, [Order::API_SHORT_CARD]);
    }

    /**
     * @Route("/list", name="list", methods={"GET"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function list()
    {
        return $this->getListResponse(
            $this->orderRepository->findAll()
        );
    }

    /**
     * @Route("/list/{productType}", name="list_by_product_type", methods={"GET"})
     *
     * @param string $productType
     * @return JsonResponse
     */
    public function listByProductType(string $productType)
    {
        return $this->getListResponse(
            $this->orderRepository->findByProductType($productType)
        );
    }

    /**
     * @param array $orders
     * @return JsonResponse
     */
    private function getListResponse(array $orders)
    {
        return $this->getResponse(
            BaseApiController::SUCCESS,
            $orders,
            Response::HTTP_OK,
            ['groups' => [Order::API_FULL_CARD, Product::API_CARD]]
        );
    }
}
