<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/products", name="products_")
 */
class ProductController extends BaseApiController
{
    /**
     * @Route("/new", name="new", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function new(Request $request): JsonResponse
    {
        return $this->create($request, new Product(), ProductType::class, [Product::API_CARD]);
    }
}
