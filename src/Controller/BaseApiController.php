<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;

class BaseApiController extends AbstractController
{
    public const MESSAGE = 'message';

    public const SUCCESS = 'Success';
    public const SERVER_ERROR = 'Server error';
    public const VALIDATION_ERROR = 'Validation error';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Gathering array of error messages by field causing it.
     *
     * @param FormInterface $form
     * @return string[]
     */
    protected function getFormErrors(FormInterface $form): array
    {
        $errors = [];
        /** @var ConstraintViolation $error */
        foreach ($form->getErrors(true) as $error) {
            $errors[$error->getCause()->getPropertyPath()][] = $error->getMessage();
        }

        return $errors;
    }

    /**
     * @param string $message
     * @param array|object|null $data
     * @param int $httpCode
     * @param array $context
     * @return JsonResponse
     */
    protected function getResponse(
        string $message,
        $data,
        int $httpCode = Response::HTTP_OK,
        array $context = []
    ): JsonResponse {
        $responseArray = [self::MESSAGE => $message];

        if ($data) {
            $responseArray = array_merge($responseArray, ['data' => $data]);
        }

        return $this->json($responseArray, $httpCode, [], $context);
    }

    /**
     * It's expected that content will be passed as json. Not adding extra checks for it yet.
     *
     * @param Request $request
     * @return mixed
     */
    protected function getContentData(Request $request)
    {
        $content = trim($request->getContent());
        $content = $content ?: '{}';

        return json_decode($content, true);
    }

    /**
     * This is not really a good place to get rid of duplicating code from order and product controllers.
     * I suppose these methods will have their own logic later. Let this method be here until we'll see how our API grows.
     *
     * @param Request $request
     * @param Order|Product $newEntity
     * @param string $formType
     * @param string[] $validationGroups
     * @return JsonResponse
     */
    protected function create(Request $request, $newEntity, string $formType, array $validationGroups)
    {
        $form = $this->createForm($formType, $newEntity);

        $form->submit($this->getContentData($request));

        if (!$form->isValid()) {
            return $this->getResponse(
                BaseApiController::VALIDATION_ERROR,
                ['errors' => $this->getFormErrors($form)],
                Response::HTTP_BAD_REQUEST
            );
        }

        try {
            $this->entityManager->persist($newEntity);
            $this->entityManager->flush();
        } catch (ORMException $exception) {
            return $this->getResponse(
                BaseApiController::SERVER_ERROR,
                null,
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $this->getResponse(
            BaseApiController::SUCCESS,
            $newEntity,
            Response::HTTP_OK,
            ['groups' => $validationGroups]
        );
    }
}
