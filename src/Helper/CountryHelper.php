<?php

namespace App\Helper;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class CountryHelper
{
    public const DEFAULT_COUNTRY_CODE = self::USA;

    private const USA = 'US';
    private const LATVIA = 'LV';

    private const STATUS_KEY = 'status';
    private const SUCCESS_CODE = 'success';
    private const COUNTRY_CODE_KEY = 'countryCode';

    // These values are just examples.
    private const DEFAULT_LIMIT = 10000;
    private const LIMITS = [
        self::DEFAULT_COUNTRY_CODE => 100,
        self::LATVIA => 1000,
    ];

    /**
     * @param Request $request
     * @return string|null
     */
    public static function getCountryByRequest(Request $request)
    {
        $httpClient = HttpClient::create();

        $requestIp = $request->getClientIp();

        if (!$requestIp) {
            return null;
        }

        $requestContent = null;
        try {
            // Possibly will add caching here.
            $response = $httpClient->request(
                'GET',
                "http://ip-api.com/json/$requestIp?fields=status,countryCode",
                [
                    'timeout' => 1
                ]
            );

            $requestContent = $response->getContent();
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface $e) {
            return null;
        }

        $content = json_decode($requestContent, true);

        if (
            !$content
            || !array_key_exists(self::STATUS_KEY, $content)
            || $content[self::STATUS_KEY] !== self::SUCCESS_CODE
            || !array_key_exists(self::COUNTRY_CODE_KEY, $content)
        ) {
            return null;
        }

        return $content[self::COUNTRY_CODE_KEY];
    }

    /**
     * @param string $countryCode
     * @return int
     */
    public static function getLimitByCountry(string $countryCode)
    {
        if (!array_key_exists($countryCode, self::LIMITS)) {
            return self::DEFAULT_LIMIT;
        }

        return self::LIMITS[$countryCode];
    }
}
