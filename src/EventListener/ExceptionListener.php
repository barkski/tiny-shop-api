<?php

namespace App\EventListener;

use App\Controller\BaseApiController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * Catching kernel exception is required because we want all generated responses made with json content-type.
 */
class ExceptionListener
{
    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $data = [BaseApiController::MESSAGE => $exception->getMessage()];
            $status = $exception->getStatusCode();
            $headers = $exception->getHeaders();
        }

        $event->setResponse(new JsonResponse(
            $data ?? [BaseApiController::MESSAGE => BaseApiController::SERVER_ERROR],
            $status ?? Response::HTTP_INTERNAL_SERVER_ERROR,
            $headers ?? []
        ));
    }
}
