<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    private const NO_COLOR = 'no color';
    private const NO_SIZE  = 'no size';

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price')
            ->add('product_type', null, [
                'property_path' => 'productType'
            ])
            ->add('color', null, [
                'empty_data' => self::NO_COLOR,
            ])
            ->add('size', null, [
                'empty_data' => self::NO_SIZE,
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
