<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(
 *     name="products",
 *     indexes={@ORM\Index(name="product_type_index", columns={"product_type"})},
 *     uniqueConstraints={@ORM\UniqueConstraint(name="product_unique_index",columns={"product_type", "color", "size"})}
 * )
 * @UniqueEntity(
 *     fields={"productType", "color", "size"},
 *     message="Combination of product type, color and size had already been used",
 *     errorPath=""
 * )
 */
class Product
{
    public const API_CARD = 'product__api_card';

    /**
     * @var int|null
     *
     * @Groups(Product::API_CARD)
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var float|null
     *
     * @Groups({Product::API_CARD, Order::API_FULL_CARD})
     *
     * @Assert\GreaterThan(0)
     *
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @var string|null
     *
     * @Groups(Product::API_CARD)
     *
     * @ORM\Column(type="string", length=255)
     */
    private $productType;

    /**
     * @var string|null
     *
     * @Groups(Product::API_CARD)
     *
     * @ORM\Column(type="string", length=255)
     */
    private $color;

    /**
     * @var string|null
     *
     * @Groups(Product::API_CARD)
     *
     * @ORM\Column(type="string", length=255)
     */
    private $size;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     * @return Product
     */
    public function setPrice(?float $price): Product
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getProductType(): ?string
    {
        return $this->productType;
    }

    /**
     * @param string|null $productType
     * @return Product
     */
    public function setProductType(?string $productType): Product
    {
        $this->productType = $productType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string|null $color
     * @return Product
     */
    public function setColor(?string $color): Product
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSize(): ?string
    {
        return $this->size;
    }

    /**
     * @param string|null $size
     * @return Product
     */
    public function setSize(?string $size): Product
    {
        $this->size = $size;
        return $this;
    }
}
