<?php

namespace App\Entity;

use App\Validator\CountryOrderCount;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\Table(name="orders", indexes={@ORM\Index(name="country_date_index", columns={"country_code", "created_at"})})
 * @CountryOrderCount()
 */
class Order
{
    public const API_SHORT_CARD = 'order__short_api_card';
    public const API_FULL_CARD = 'order__full_api_card';

    /**
     * @var int
     *
     * @Groups({Order::API_SHORT_CARD, Order::API_FULL_CARD})
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var OrderItem[]|ArrayCollection
     *
     * @Groups(Order::API_FULL_CARD)
     *
     * @Assert\Valid()
     * @Assert\Count(min="1")
     *
     * @ORM\OneToMany(targetEntity="App\Entity\OrderItem", mappedBy="order", orphanRemoval=true, cascade={"persist"})
     */
    private $orderItems;

    /**
     * @var string|null
     *
     * @Groups({Order::API_SHORT_CARD, Order::API_FULL_CARD})
     *
     * @ORM\Column(name="country_code", type="string", length=255)
     */
    private $countryCode;

    /**
     * @var DateTime|null
     *
     * @Groups(Order::API_FULL_CARD)
     * @SerializedName("create_date")
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
        $this->createdAt = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    /**
     * @param OrderItem $orderItem
     * @return Order
     */
    public function addOrderItem(OrderItem $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems[] = $orderItem;
            $orderItem->setOrder($this);
        }

        return $this;
    }

    /**
     * @param OrderItem $orderItem
     * @return Order
     */
    public function removeOrderItem(OrderItem $orderItem): self
    {
        if ($this->orderItems->contains($orderItem)) {
            $this->orderItems->removeElement($orderItem);
            // set the owning side to null (unless already changed)
            if ($orderItem->getOrder() === $this) {
                $orderItem->setOrder(null);
            }
        }

        return $this;
    }

    /**
     * This method is used in API response. I suppose we will store calculated price in database later.
     *
     * @Groups(Order::API_SHORT_CARD)
     * @Assert\GreaterThanOrEqual(10)
     *
     * @return int
     */
    public function getPrice(): int
    {
        $price = 0;

        foreach ($this->orderItems as $orderItem) {
            if (!$orderItem->getProduct()) {
                continue;
            }

            $price += $orderItem->getProduct()->getPrice() * $orderItem->getQuantity();
        }

        return $price;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    /**
     * @param string|null $countryCode
     * @return Order
     */
    public function setCountryCode(?string $countryCode): Order
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**

     *
     * @return string|null
     */
    public function getCreatedDate(): ?string
    {
        return $this->createdAt->format(DateTime::ISO8601);
    }

    /**
     * @param DateTime|null $createdAt
     * @return Order
     */
    public function setCreatedAt(?DateTime $createdAt): Order
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
